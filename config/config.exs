# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :gotime,
  namespace: GoTime,
  ecto_repos: [GoTime.Repo]

# Configures the endpoint
config :gotime, GoTimeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "WosUlFUYW6QPO4xx/T2Bc1SWSdK7SlGy9379NWEto19p3ugG7Cr8BBXiMXBZQoCC",
  render_errors: [view: GoTimeWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: GoTime.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
