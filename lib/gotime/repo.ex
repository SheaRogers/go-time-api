defmodule GoTime.Repo do
  use Ecto.Repo,
    otp_app: :gotime,
    adapter: Ecto.Adapters.Postgres
end
