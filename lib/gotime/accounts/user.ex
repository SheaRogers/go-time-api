defmodule GoTime.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :bio, :string
    field :email, :string
    field :image, :string
    field :password, :string
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password, :username, :bio, :image])
    |> validate_required([:email, :password, :username, :bio, :image])
    |> unique_constraint(:email)
  end
end
