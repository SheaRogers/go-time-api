defmodule GoTimeWeb.Router do
  use GoTimeWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", GoTimeWeb do
    pipe_through :api
  end
end
